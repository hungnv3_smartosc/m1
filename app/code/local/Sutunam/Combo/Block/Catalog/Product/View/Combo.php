<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Catalog_Product_View_Combo extends Mage_Catalog_Block_Product_Abstract
{
    /** @var Sutunam_Combo_Model_Repository_ComboRepository $comboRepository */
    protected $comboRepository;

    protected $currentProduct;

    protected function _construct()
    {
        $this->comboRepository = Mage::getModel('sutunam_combo/repository_comboRepository');
        $this->currentProduct = Mage::registry('current_product');
    }

    public function getComboById()
    {
        return $this->comboRepository->getByProductId($this->currentProduct->getId());
    }


}