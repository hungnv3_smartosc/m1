<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Adminhtml_Combo_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('sutunam_combo_form');
        $this->setTitle(Mage::helper('sutunam_combo')->__('Combo Details'));
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('adminhtml/adminhtml_combo/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}