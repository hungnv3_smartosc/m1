<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Adminhtml_Combo_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = Sutunam_Combo_Model_Combo::COMBO_ID;
        $this->_blockGroup = 'sutunam_combo';
        $this->_controller = 'adminhtml_combo';
        parent::__construct();
        $this->_updateButton('save', 'label', Mage::helper('sutunam_combo')->__('Save Combo'));
        $this->_updateButton('delete', 'label', Mage::helper('sutunam_combo')->__('Delete Combo'));
        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Getter for form header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        $combo = Mage::registry('current_combo');
        if ($combo->getComboId()) {
            return Mage::helper('sutunam_combo')->__("Edit '%s'", $this->escapeHtml($combo->getComboName()));
        }
        else {
            return Mage::helper('sutunam_combo')->__('New Combo');
        }
    }
}