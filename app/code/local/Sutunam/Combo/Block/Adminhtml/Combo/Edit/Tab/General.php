<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Adminhtml_Combo_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        /** @var Sutunam_Combo_Model_Combo $model */
        $model = Mage::registry('current_combo');

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('general_fs',
            array('legend' => Mage::helper('sutunam_combo')->__('Combo information')));

        $fieldset->addField(Sutunam_Combo_Model_Combo::COMBO_NAME, 'text', array(
            'label' => Mage::helper('sutunam_combo')->__('Combo Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => Sutunam_Combo_Model_Combo::COMBO_NAME,
        ));

        if (!empty($model) && $model->getId()) {
            $fieldset->addField(Sutunam_Combo_Model_Combo::COMBO_ID, 'hidden', array(
                'name' => Sutunam_Combo_Model_Combo::COMBO_ID,
            ));
        }

        if (!empty($model)) {
            $form->setValues($model->getData());
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }
}