<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Adminhtml_Combo_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sutunamComboGrid');
        $this->setDefaultSort(Sutunam_Combo_Model_Combo::COMBO_ID);
        $this->setDefaultDir('DESC');
    }

    public function getRowUrl($row)
    {
        //return $this->getUrl('*/*/edit', array(Sutunam_Combo_Model_Combo::COMBO_ID => $row->getId()));
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sutunam_combo/combo')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(Sutunam_Combo_Model_Combo::COMBO_ID, array(
            'header' => Mage::helper('sutunam_combo')->__('Combo ID'),
            'type' => 'number',
            'index' => Sutunam_Combo_Model_Combo::COMBO_ID
        ));

        $this->addColumn(Sutunam_Combo_Model_Combo::COMBO_NAME, array(
            'header' => Mage::helper('sutunam_combo')->__('Combo Name'),
            'type' => 'text',
            'index' => Sutunam_Combo_Model_Combo::COMBO_NAME
        ));

        $this->addColumn(Sutunam_Combo_Model_Combo::UPDATED_AT, array(
            'header' => Mage::helper('sutunam_combo')->__('Updated'),
            'type' => 'date',
            'index' => Sutunam_Combo_Model_Combo::UPDATED_AT
        ));

        $this->addColumn(Sutunam_Combo_Model_Combo::CREATED_AT, array(
            'header' => Mage::helper('sutunam_combo')->__('Created'),
            'type' => 'date',
            'index' => Sutunam_Combo_Model_Combo::CREATED_AT
        ));

        $this->addColumn('action', array(
            'header' =>  Mage::helper('sutunam_combo')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'actions'   => array(
                array(
                    'caption'   => Mage::helper('sutunam_combo')->__('Edit'),
                    'url'       => $this->getUrl('*/*/edit/combo_id/$combo_id'),
                )
            )
        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField(Sutunam_Combo_Model_Combo::COMBO_ID);
        $this->getMassactionBlock()->setFormFieldName('combo_ids');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('sutunam_combo')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('sutunam_combo')->__('Are you sure?')
        ));

        return $this;
    }


}