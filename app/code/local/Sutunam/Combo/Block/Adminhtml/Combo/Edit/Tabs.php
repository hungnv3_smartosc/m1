<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Adminhtml_Combo_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('form_tabs');
        $this->setDestElementId('edit_form'); // this should be same as the form id define above
        $this->setTitle(Mage::helper('sutunam_combo')->__('Combo Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('sutunam_combo')->__('General Information'),
            'title'     => Mage::helper('sutunam_combo')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('sutunam_combo/adminhtml_combo_edit_tab_general')->toHtml(),
        ));

        $comboId = Mage::registry('current_combo') ? Mage::registry('current_combo')->getComboId() : null;;
        $this->addTab('product_section', array(
            'label'     => Mage::helper('sutunam_combo')->__('Products'),
            'title'     => Mage::helper('sutunam_combo')->__('Products'),
            'url'       => $this->getUrl('*/adminhtml_combo_ajax/getProduct', array('_current' => true, Sutunam_Combo_Model_Combo::COMBO_ID => $comboId)),
            'class'     => 'ajax',
        ));

        return parent::_beforeToHtml();
    }
}