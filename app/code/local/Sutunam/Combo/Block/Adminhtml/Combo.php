<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Block_Adminhtml_Combo extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'sutunam_combo';
        $this->_controller = 'adminhtml_combo';
        $this->_headerText = Mage::helper('sutunam_combo')->__('Combo Management');
        $this->_addButtonLabel = Mage::helper('sutunam_combo')->__('Add Combo');
        parent::__construct();
    }
}