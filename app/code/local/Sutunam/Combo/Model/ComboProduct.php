<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_ComboProduct extends Mage_Core_Model_Abstract
{
    const ID = 'id';
    const COMBO_ID = 'combo_id';
    const PRODUCT_ID = 'product_id';
    const POSITION = 'position';

    private $product;

    protected function _construct()
    {
        $this->_init('sutunam_combo/comboProduct');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->setData(self::ID, $id);
        return $this;
    }

    /**
     * @return int
     */
    public function getComboId()
    {
        return $this->getData(self::COMBO_ID);
    }

    /**
     * @param mixed $comboId
     * @return self
     */
    public function setComboId($comboId)
    {
        $this->setData(self::COMBO_ID, $comboId);
        return $this;
    }


    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @param int $productId
     * @return self
     */
    public function setProductId($productId)
    {
        $this->setData(self::PRODUCT_ID, $productId);
        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->getData(self::POSITION);
    }

    /**
     * @param int $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->setData(self::POSITION, $position);
        return $this;
    }


    public function addProduct($product) {
        $this->product = $product;
        return $this;
    }

    public function getProduct() {
        return $this->product;
    }


}