<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_Resource_Combo_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('sutunam_combo/combo');
    }
}
