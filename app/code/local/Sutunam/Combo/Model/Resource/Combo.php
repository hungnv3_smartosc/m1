<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_Resource_Combo extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('sutunam_combo/combo', 'combo_id');
    }

    public function getComboIdByProductId($productId)
    {
        $connection = $this->_getReadAdapter();

        // query to delete
        $select = $connection->select()
        ->from($this->getTable('sutunam_combo/combo_product'), array('combo_id'))
        ->where('product_id = ?', $productId);

        return $connection->fetchAll($select);
    }
}