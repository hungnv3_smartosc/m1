<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_Resource_ComboProduct extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('sutunam_combo/combo_product', 'id');
    }

    /**
     * @param $comboId
     */
    public function deleteByComboId($comboId) {
        $connection = $this->_getWriteAdapter();

        // query to delete
        $connection->delete($this->getMainTable(), array(
            'combo_id = ?' => $comboId,
        ));
    }

    /**
     * @param $comboProductsData
     * @throws Zend_Db_Adapter_Exception
     */
    public function insertValues($comboProductsData) {
        $connection = $this->_getWriteAdapter();
        $connection->insertMultiple($this->getMainTable(), $comboProductsData);
    }
}