<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_Repository_ComboProductRepository
{

    /**
     * @var Sutunam_Combo_Model_Resource_ComboProduct
     */
    private $resource;

    public function __construct()
    {
        $this->resource = Mage::getResourceModel('sutunam_combo/comboProduct');
    }

    /**
     * @param int $comboId
     * @return array
     */
    public function loadByComboId($comboId)
    {
        /** @var Sutunam_Combo_Model_Resource_ComboProduct_Collection $collection */
        $collection = Mage::getModel('sutunam_combo/comboProduct')->getCollection();
        $collection
            ->addFieldToSelect(array('id', 'product_id', 'combo_id', 'position'))
            ->addFieldToFilter('combo_id', $comboId)
            ->load();
        return $collection->getItems();
    }

    /**
     * @param int $comboId
     * @throws Mage_DB_Exception
     */
    public function deleteByComboId($comboId)
    {
        $this->resource->deleteByComboId($comboId);
    }

    /**
     * @param Sutunam_Combo_Model_Combo $combo
     * @throws Mage_DB_Exception
     */
    public function saveForCombo($combo)
    {
        if (empty($combo->getComboProducts())) {
            throw new Mage_DB_Exception(Mage::helper('adminhtml')->__('Combo products is empty'));
        }

        // save combo product
        $comboProductsData = [];
        /** @var Sutunam_Combo_Model_ComboProduct $comboProduct */
        foreach ($combo->getComboProducts() as $comboProduct) {
            $comboProductData = $comboProduct->getData();
            if (empty($comboProductData['combo_id'])) {
                $comboProductData['combo_id'] = $combo->getId();
            }
            $comboProductsData[] = $comboProductData;
        }
        if (count($comboProductsData) > 0) {
            $this->resource->insertValues($comboProductsData);
        }
    }

    public function loadProduct($comboProducts)
    {
        $productId= array();
        foreach ($comboProducts as $comboproduct) {
            $productId[] = $comboproduct->getProductId();
        }
        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('entity_id', array('in'=> $productId))->addAttributeToSelect('*');
        return $productCollection->load()->getItems();

    }

}