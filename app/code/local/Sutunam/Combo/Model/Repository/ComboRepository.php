<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_Repository_ComboRepository
{
    /** @var Sutunam_Combo_Model_Resource_Combo */
    private $resource;

    /** @var array */
    private $cachedById = array();

    public function __construct()
    {
        $this->resource = Mage::getResourceModel('sutunam_combo/combo');
    }

    public function save($combo)
    {
       $combo->save();
    }

    public function deleteById($comboID)
    {
        $combo = $this->getById($comboID);
        $combo->delete();
    }

    public function delete($combo)
    {
        $combo->delete();
    }

    /**
     * @param $comboID
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getById($comboId)
    {
        if (empty($this->cachedById[$comboId])) {
            /** @var Sutunam_Combo_Model_Combo $combo */
            $combo = Mage::getModel('sutunam_combo/combo');

            $combo->load($comboId);

            if (empty($combo->getId())) {
                throw new Mage_DB_Exception(Mage::helper('adminhtml')->__('Combo does not exist'));
            }

            $this->cachedById[$comboId] = $combo;
        }

        return $this->cachedById[$comboId];
    }

    /**
     * @param $productId
     * @return array
     */
    public function getByProductId($productId) {
        $comboIds = $this->resource->getComboIdByProductId($productId);
        $result = array();
        foreach ($comboIds as $arr) {
            /** @var Sutunam_Combo_Model_Combo $combo */
            $combo = Mage::getModel('sutunam_combo/combo')->load($arr['combo_id']);
            $result[] = $combo;
        }

        return $result;

    }

}