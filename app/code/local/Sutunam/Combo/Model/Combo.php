<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

/**
 * Class Sutunam_Combo_Model_Combo
 */
class Sutunam_Combo_Model_Combo extends Mage_Core_Model_Abstract
{
    const COMBO_ID = 'combo_id';
    const COMBO_NAME = 'combo_name';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $_eventPrefix = 'combo';

    /**
     * @var Sutunam_Combo_Model_ComboProduct[]
     */
    private $comboProducts = array();

    private $products = array();

    protected function _construct()
    {
        $this->_init('sutunam_combo/combo');
    }

    /**
     * @return int
     */
    public function getComboId()
    {
        return $this->getData(self::COMBO_ID);
    }

    /**
     * @param $comboId
     * @return self
     */
    public function setComboId($comboId)
    {
        $this->setData(self::COMBO_ID, $comboId);
        return $this;
    }

    /**
     * @return string
     */
    public function getComboName()
    {
        return $this->getData(self::COMBO_NAME);
    }

    /**
     * @param $comboName
     * @return self
     */
    public function setComboName($comboName)
    {
        $this->setData(self::COMBO_NAME, $comboName);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @param $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(self::CREATED_AT, $createdAt);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * @param $createdAt
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(self::UPDATED_AT, $updatedAt);
        return $this;
    }

    public function getComboProducts()
    {
        return $this->comboProducts;
    }

    public function setComboProducts($comboProducts)
    {
        $this->comboProducts = $comboProducts;
        return $this;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    public function removeComboProducts()
    {
        $this->comboProducts = array();
        return $this;
    }

    /**
     * @param Sutunam_Combo_Model_ComboProduct $comboProduct
     * @return self
     */
    public function addComboProduct($comboProduct)
    {
        $this->comboProducts[] = $comboProduct;
        return $this;
    }
}