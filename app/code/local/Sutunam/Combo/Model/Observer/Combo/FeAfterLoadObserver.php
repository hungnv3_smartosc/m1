<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */


class Sutunam_Combo_Model_Observer_Combo_FeAfterLoadObserver extends Varien_Object
{

    public function loadComboProducts($observer)
    {
        $event = $observer->getEvent();

        /** @var Sutunam_Combo_Model_Combo $combo */
        $combo = $event->getObject();
        if ($combo->getComboId()) {
            /** @var Sutunam_Combo_Model_Repository_ComboProductRepository $comboProductRepository */
            $comboProductRepository = Mage::getModel('sutunam_combo/repository_comboProductRepository');
            $comboProducts = $comboProductRepository->loadByComboId($combo->getComboId());
            $products = $comboProductRepository->loadProduct($comboProducts);
            $combo->setComboProducts($comboProducts);
            $combo->setProducts($products);
        }
    }
}