<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Model_Observer_Combo_AfterSaveObserver extends Varien_Object
{
    public function saveComboProducts($observer)
    {
        $event = $observer->getEvent();

        /** @var Sutunam_Combo_Model_Combo $combo */
        $combo = $event->getObject();
        if (empty($combo->getComboId())) {
            throw new Mage_DB_Exception(Mage::helper('adminhtml')->__('Combo does not exist'));
        }

        /** @var Sutunam_Combo_Model_Repository_ComboProductRepository $comboProductRepository */
        $comboProductRepository = Mage::getModel('sutunam_combo/repository_comboProductRepository');
        $comboProductRepository->deleteByComboId($combo->getComboId());
        $comboProductRepository->saveForCombo($combo);
    }
}