<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

/**
 * Create table 'sutunam1_blog_blog'
 */

$tableBlog = $installer->getConnection()
    ->newTable($installer->getTable('sutunam_combo/combo'))
    ->addColumn(Sutunam_Combo_Model_Combo::COMBO_ID, Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Combo Id')
    ->addColumn(
        Sutunam_Combo_Model_Combo::COMBO_NAME,
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array('nullable' => false),
        'Combo Name'
    )
    ->addColumn(
        Sutunam_Combo_Model_Combo::CREATED_AT,
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT),
        'Created At'
    )
    ->addColumn(
        Sutunam_Combo_Model_Combo::UPDATED_AT,
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array('nullable' => false, 'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE),
        'Updated At'
    );

$installer->getConnection()->createTable($tableBlog);


$tableBlog = $installer->getConnection()
    ->newTable($installer->getTable('sutunam_combo/combo_product'))
    ->addColumn(Sutunam_Combo_Model_ComboProduct::ID, Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Id')
    ->addColumn(Sutunam_Combo_Model_ComboProduct::COMBO_ID, Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default' => '0',
    ), 'Combo Id')
    ->addColumn(Sutunam_Combo_Model_ComboProduct::PRODUCT_ID, Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default' => '0',
    ), 'Product Id')
    ->addColumn(Sutunam_Combo_Model_ComboProduct::POSITION, Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default' => '0',
    ), 'Position')
    ->addForeignKey($installer->getFkName(
        'sutunam_combo/combo_product',
        'combo_id',
        'sutunam_combo/combo',
        'combo_id'
    ),
        'combo_id',
        $installer->getTable('sutunam_combo/combo'),
        'combo_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey($installer->getFkName(
        'sutunam_combo/combo_product',
        'product_id',
        'catalog/product',
        'entity_id'
    ),
        'product_id',
        $installer->getTable('catalog/product'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    );


$installer->getConnection()->createTable($tableBlog);

$installer->endSetup();


