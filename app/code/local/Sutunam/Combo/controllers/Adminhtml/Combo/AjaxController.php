<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

require_once(Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Catalog' . DS . 'ProductController.php');

class Sutunam_Combo_Adminhtml_Combo_AjaxController extends Mage_Adminhtml_Catalog_ProductController
{
    /** @var Sutunam_Combo_Model_Repository_ComboRepository $comboRepository */
    private $comboRepository;

    protected function _construct()
    {
        parent::_construct(); // TODO: Change the autogenerated stub
        $this->comboRepository = Mage::getModel('sutunam_combo/repository_comboRepository');
    }

    /**
     * Get customd products grid and serializer block
     */
    public function getProductAction()
    {

        $this->_initCombo();
        $this->loadLayout();

        /** @var  $gridAjaxBlock Sutunam_Combo_Block_Adminhtml_Combo_Edit_Tab_Ajax_Product */
        $gridAjaxBlock = $this->getLayout()->getBlock('ajax_product_grid');
        $gridAjaxBlock->setPostComboProducts($this->getRequest()->getPost('post_combo_products', null));

        $this->renderLayout();
    }

    /**
     * Get custom products grid
     */
    public function renderGridAction()
    {
        $this->_initCombo();
        $this->loadLayout();

        /** @var  $gridAjaxBlock Sutunam_Combo_Block_Adminhtml_Combo_Edit_Tab_Ajax_Product */
        $gridAjaxBlock = $this->getLayout()->getBlock('ajax_product_grid');
        $gridAjaxBlock->setPostComboProducts($this->getRequest()->getPost('post_combo_products', null));

        $this->renderLayout();
    }


    protected function _initCombo()
    {
        $comboId = $this->getRequest()->getParam('combo_id');
        if ($comboId) {
            /** @var Sutunam_Combo_Model_Combo $combo */
            $combo = $this->comboRepository->getById($comboId);
            Mage::register('current_combo', $combo);
        }
    }

}