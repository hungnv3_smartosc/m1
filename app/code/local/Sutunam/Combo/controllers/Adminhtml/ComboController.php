<?php
/**
 * Sutunam Blog
 *
 * @copyright  Copyright (c) 2019 Sutunam (http://www.sutunam.com/)
 */

class Sutunam_Combo_Adminhtml_ComboController extends Mage_Adminhtml_Controller_Action
{
    /** @var Sutunam_Combo_Model_Repository_ComboRepository $comboRepository */
    private $comboRepository;

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $comboID = $this->getRequest()->getParam('combo_id');

        $combo = $this->getCombo($comboID);

        $this->_title(
            $combo->getId() ?
                $combo->getComboName() :
                $this->__('New Combo')
        );

        //Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $combo->setData($data);
        }

        //Register model to use later in blocks
        Mage::register('current_combo', $combo);

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function getCombo($comboId)
    {
        $combo = null;
        if ($comboId) {
            /** @var Sutunam_Combo_Model_Combo $combo */
            $combo = $this->comboRepository->getById($comboId);

            if (!$combo->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('sutunam_combo')->__('This combo no longer exists.')
                );
                $this->_redirect('*/*/index');
                return;
            }
        }

        if (!$combo instanceof Sutunam_Combo_Model_Combo) {
            $combo = Mage::getModel('sutunam_combo/combo');
        }

        return $combo;
    }

    public function deleteAction()
    {
        $comboID = $this->getRequest()->getParam('combo_id');
        $combo = $this->getCombo($comboID);
        try {
            $this->comboRepository->delete($combo);
        } catch (Exception $e) {
            // display error message
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            // redirect to edit form
            $this->_redirect('*/*/edit', array('combo_id' => $this->getRequest()->getParam('combo_id')));
            return;
        }
        $this->_redirect('*/*/index');
    }

    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $comboID = !empty($data['combo_id']) ? $data['combo_id'] : null;
            $combo = $this->getCombo($comboID);

            try {
                // set general
                $combo->setComboName($data['combo_name']);

                // Parse combo product
                if (!empty($data['selected_products'])) {
                    $productsData = Mage::helper('adminhtml/js')->decodeGridSerializedInput($data['selected_products']);

                    $combo->removeComboProducts();

                    foreach ($productsData as $productId => $productData) {
                        /** @var Sutunam_Combo_Model_ComboProduct $comboProduct */
                        $comboProduct = Mage::getModel('sutunam_combo/comboProduct');
                        // we dont need to set combo_id, combo_product repository will do it if combo_id is null
                        $comboProduct->setProductId($productId);
                        $comboProduct->setPosition($productData['position']);
                        $combo->addComboProduct($comboProduct);
                    }
                }

                // save Combo
                $this->comboRepository->save($combo);

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('sutunam_combo')->__('The Combo has been saved.')
                );
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('combo_id' => $combo->getComboId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/index');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('combo_id' => $this->getRequest()->getParam('combo_id')));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massDeleteAction() {
        $comboIds = $this->getRequest()->getParam('combo_ids');
        if(!is_array($comboIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select combo(s)'));
        } else {
            try {
                foreach ($comboIds as $comboId) {
                    $this->comboRepository->deleteById($comboId);
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d combo(s) were successfully deleted', count($comboIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _construct()
    {
        parent::_construct(); // TODO: Change the autogenerated stub
        $this->comboRepository = Mage::getModel('sutunam_combo/repository_comboRepository');
    }

    protected function __isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sutunam_combo');
    }
}